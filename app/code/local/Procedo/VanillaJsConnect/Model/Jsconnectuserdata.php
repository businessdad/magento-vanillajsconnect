<?php

class Procedo_VanillaJsConnect_Model_JsConnectUserData {
  /**
   * return signature
   *
   * @param string $string
   * @return string
   */
  public function signature($string) {
    return md5($string);
  }

  /**
   * builds user array
   *
   * @todo return false/null when not logged in instead of an empty array
   * @return array
   */
  public function getUserData() {
    if(!Mage::getSingleton('customer/session')->isLoggedIn()) {
      return null;
    }

    $customer = Mage::getSingleton('customer/session')->getCustomer();

		// If Customer has a User Name associated to it, take it. If not, take his
		// full name
		$userName = $customer->getUsername();
		if(empty($userName)) {
			$userName = $customer->getName();
		}

    $data = array(
      'uniqueid' => $customer->getId(),
      'name' => $userName,
      'email' => $customer->getEmail(),
      'photourl' => '',
    );

    return $data;
  }
}
