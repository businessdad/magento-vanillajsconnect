<?php
require(Mage::getModuleDir('', 'Procedo_VanillaJsConnect') . DS . 'includes' . DS .'functions.jsconnect.php');

define('REQUEST_CLIENT_ID', 'client_id');
define('REQUEST_SIGNATURE', 'signature');
define('REQUEST_TIMESTAMP', 'timestamp');
define('REQUEST_CALLBACK', 'callback');

class Procedo_VanillaJsConnect_IndexController extends Mage_Core_Controller_Front_Action {
	/**
	 * Equivalent of SQL Coalesce function, it returns the first non-null
	 * argument.
	 *
	 * @param Any amount of arguments.
	 * @result The value of the first non-null argument, or null if they are all
	 * null.
	 */
	protected function coalesce() {
		return array_shift(array_filter(func_get_args()));
	}

	/**
	 * Retrieves the details of currently logged in User and prepares a JSON
	 * output for Vanilla, to enable single sign-on.
	 *
	 */
	public function indexAction() {
		$clientId = Mage::getStoreConfig('vanillajsconnect/connectionsettings/client_id');
		$clientSecret = Mage::getStoreConfig('vanillajsconnect/connectionsettings/client_secret');

		$requestParams = array();
		$requestParams[REQUEST_CLIENT_ID] = $this->getRequest()->getParam(REQUEST_CLIENT_ID, null);
		$requestParams[REQUEST_SIGNATURE] = $this->getRequest()->getParam(REQUEST_SIGNATURE, null);
		$requestParams[REQUEST_TIMESTAMP] = $this->getRequest()->getParam(REQUEST_TIMESTAMP, null);
		$requestParams[REQUEST_CALLBACK] = $this->getRequest()->getParam(REQUEST_CALLBACK, null);

		$model = Mage::getModel('vanillajsconnect/jsconnectuserdata');
		$userData = $model->getUserData();

		$secure = true;

		// Output JsConnect Data
		WriteJsConnect($userData, $requestParams, $clientId, $clientSecret, $secure);
	}
}
